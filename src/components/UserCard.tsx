import { Image, Text, View } from 'react-native';
import { Result } from 'user';

const UserCard: React.FunctionComponent<Result> = ({ picture, name, location, email }) => {
	return (
		<View style={{ backgroundColor: 'white', padding: 5, borderRadius: 5, flexDirection: 'row', alignItems: 'center', marginBottom: 10 }}>
			<View>
				<Image source={{ uri: picture.large }} style={{ width: 70, height: 70, borderRadius: 5 }} />
			</View>
			<View style={{ marginLeft: 10 }}>
				<Text style={{ fontSize: 18, fontWeight: '500' }}>
					{name.first} {name.last}
				</Text>
				<Text>
					{location.street.name} {location.street.number} {location.city}
				</Text>
				<Text style={{ color: '#ec4899' }}>{email}</Text>
			</View>
		</View>
	);
};

export default UserCard;
