import { StatusBar } from 'expo-status-bar';
import { useEffect, useState } from 'react';
import { FlatList, ScrollView, StyleSheet, Text, View, Dimensions, SafeAreaView } from 'react-native';
import { Result } from 'user';
import UserCard from './src/components/UserCard';

export default function App() {
	const [users, setUsers] = useState<Result[]>([]);
	const [amount, setAmount] = useState<number>(10);

	useEffect(() => {
		(async () => {
			const res = await fetch(`https://randomuser.me/api/?results=${amount}`);
			const data = await res.json();

			setUsers(data.results);
		})();
	}, [amount]);

	return (
		<SafeAreaView style={[styles.container]}>
			{/* <StatusBar style="auto" /> */}
			<View style={styles.layout}>
				<Text style={styles.titleText}>Customers</Text>
			</View>
			<View style={styles.flexOne}>
				<ScrollView style={[styles.layout, { marginTop: 20 }]}>
					<FlatList
						data={users}
						onEndReached={() => setAmount(prev => prev + 10)}
						style={{ height: Dimensions.get('screen').height - 80 }}
						renderItem={({ item: user }) => <UserCard {...user} key={user.id.value} />}
					/>
				</ScrollView>
			</View>
		</SafeAreaView>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#14b8a6',
		maxWidth: 1024,
		width: '100%',
		alignSelf: 'center',
	},
	layout: {
		width: '90%',
		alignSelf: 'center',
	},
	titleText: { color: 'white', fontSize: 24, fontWeight: '600', marginTop: 10 },
	flexOne: { flex: 1 },
});
